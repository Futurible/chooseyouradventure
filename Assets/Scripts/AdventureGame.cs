﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Linq;

public class AdventureGame : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI textComponent;
    [SerializeField] State stage_0;
    [SerializeField] Message messagesIndex;
    [SerializeField] GameObject messageObject;
    [SerializeField] TextMeshProUGUI messageComponent;
    [SerializeField] GameObject compassObject;

    [Header("Inventory Count")]
    public TextMeshProUGUI flareCount;
    public TextMeshProUGUI pistolCount;
    public TextMeshProUGUI bulletsCount;
    public TextMeshProUGUI compassCount;
    public TextMeshProUGUI counterText;

    [Header("Content management")]
    public ScriptableObject flareUse;
    public ScriptableObject[] bulletUse;
    public ScriptableObject pickCompass;
    public ScriptableObject compassUse;
    public ScriptableObject[] endPerishByBullet;
    public ScriptableObject[] endPerishByNature;

    [Header("Buttons")]
    public Button backbutton;

    private int flareValue = 1;
    private int pistolValue = 1;
    private int bulletsValue = 3;
    private int compassValue = 0;
    private int counter = 0;

    State state;
    Message message;
    List<State> recordedStates = new List<State>();
    List<Message> recordedMessages = new List<Message>();
    List<int> recordedInventoryItemFlare = new List<int>();
    List<int> recordedInventoryItemBullets = new List<int>();
    List<int> recordedInventoryItemCompass = new List<int>();

    // Use this for initialization
    void Start()
    {
        state = stage_0;
        textComponent.text = state.GetStateStory();
        message = messagesIndex;
        messageComponent.text = message.GetMessageTxt();
        backbutton.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        ManageState();
        ManageText();

    }

    private void ManageState()
    {

        /*  Original function

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                StateActions(0);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                StateActions(1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                StateActions(2);
            }
            textComponent.text = state.GetStateStory();

        */
        var stateOptions = state.GetNextStates();
        for (int index = 0; index < stateOptions.Length; index++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1 + index))
            {
                StateActions(index);
            }
        }
    }

    private void ManageText()
    {
        flareCount.text = flareValue.ToString();
        pistolCount.text = pistolValue.ToString();
        bulletsCount.text = bulletsValue.ToString();
        compassCount.text = compassValue.ToString();
        counterText.text = counter.ToString();
    }

    public void StateActions(int optionChoosen)
    {
        var nextStates = state.GetNextStates();
        MovesRecorder();
        state = nextStates[optionChoosen];
        counter++;
        ManageInventory();
        backbutton.interactable = (counter == 0) ? false : true;
        Debug.Log(state);
    }

    public void MovesRecorder()
    {
        recordedStates.Add(state);
        recordedMessages.Add(message);
        recordedInventoryItemFlare.Add (flareValue);
        recordedInventoryItemBullets.Add(bulletsValue);
        recordedInventoryItemCompass.Add(compassValue);
        Debug.Log(state);
    }

    public void ManageInventory()
    {
        var stateMessage = message.GetMessage();

        // If I have the flare I'll be able to use it
        if (state == flareUse)
        {
            if (flareValue > 0)
                flareValue--;

            else if (flareValue == 0)
                StateProgressBlock(0);
        }

        // For as long as I have bullets I'll be able to use them
        else if (bulletUse.Contains(state))
        {
            if (bulletsValue > 0)
            {
                bulletsValue--;
                if (endPerishByBullet.Contains(state))
                    message = stateMessage[4];
            }
            else if (bulletsValue == 0)
                StateProgressBlock(1);
        }

        // If I don't have a compass, it will be added.
        else if (state == pickCompass)
        {
            if (compassValue == 0)
            {
                compassValue++;
                message = stateMessage[2];
                compassObject.gameObject.SetActive(true);
            }
        }

        // If I have the compass I'll be able to use it.
        else if (state == compassUse)
        {
            if (compassValue == 1)
                message = stateMessage[4];
            else
                StateProgressBlock(3);
        }

        else if (endPerishByNature.Contains(state))
        {
            message = stateMessage[4];
        }

        messageComponent.text = message.GetMessageTxt();
        message = messagesIndex;
    }

    private void StateProgressBlock(int noItemMessage)
    {
        var blockMessage = message.GetMessage();

        PreviousState();
        message = blockMessage[noItemMessage];
        messageComponent.text = message.GetMessageTxt();
    }

    private void Restart()
    {
        counter = 0;
        Start();
        flareValue = recordedInventoryItemFlare[counter];
        bulletsValue = recordedInventoryItemBullets[counter];
        compassValue = recordedInventoryItemCompass[counter];
        compassObject.gameObject.SetActive(false);
    }

    private void PreviousState()
    {
        counter--;
        state = recordedStates[counter];
        message = recordedMessages[counter];
        flareValue = recordedInventoryItemFlare[counter];
        bulletsValue = recordedInventoryItemBullets[counter];
        compassValue = recordedInventoryItemCompass[counter];
        if (compassValue == 0) compassObject.gameObject.SetActive(false);
        textComponent.text = state.GetStateStory();
        messageComponent.text = message.GetMessageTxt();
        recordedStates.Remove(state);
        recordedMessages.Remove(message);
        recordedInventoryItemFlare.Remove(flareValue);
        recordedInventoryItemBullets.Remove(bulletsValue);
        recordedInventoryItemCompass.Remove(compassValue);

        if (counter == 0)
            backbutton.interactable = false;
    }       

}
