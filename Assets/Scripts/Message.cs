﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Message")]
public class Message : ScriptableObject {

    // TextArea will create a proper text box 5 units (min_width min_lenght)
    [TextArea(5,5)][SerializeField] string messageText;
    [SerializeField] Message[] stateMessage;

    public string GetMessageTxt()
    {
        return messageText;
    }

    public Message[] GetMessage()
    {
        return stateMessage;
    }
}
