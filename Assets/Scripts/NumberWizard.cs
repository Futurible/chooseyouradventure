﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour {

    [SerializeField] int maxNumber;
    [SerializeField] int minNumber;
    [SerializeField] TextMeshProUGUI guessText;
    int guess;

    // Use this for initialization

    void Start()
    {
        StartGame();
    }

    void StartGame() {

        NextGuess();
    }
	
	// Update is called once per frame
	void Update ()
    {
       
	}

    public void OnPressHigher()
    {
        minNumber = guess + 1;
        NextGuess();
    }

    public void OnPressLower()
    {
        maxNumber = guess - 1;
        NextGuess();
    }

    void NextGuess()
    {
        guess = Random.Range(minNumber, maxNumber + 1);
        guessText.text = guess.ToString();
        Debug.Log("Is it higher or lower than " + guess);
    }
}
